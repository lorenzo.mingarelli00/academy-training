'use strict';

var server = require('server');
var site = require('dw/system/Site');
var productMgr = require('dw/catalog/ProductMgr');
var productImport = require('dw/catalog/Product');
var logger = require('dw/system/Logger');

server.get(
    'Show',
    server.middleware.https,
    function (req, res, next){
        var customLogger = logger.getLogger('error');

        if (site.getCurrent().getCustomPreferenceValue("isAcademyProductActive")) {
            var id = req.querystring.id;

            if (id != null) {
                var product = productMgr.getProduct(id);

                var datas = {};
                datas = dw.system.HookMgr.callHook( "app.hookacademy", "productData", product );
                res.render('/academyProduct/academyproduct', datas);
            }else{
                customLogger.error('isAcademyProductActive == null');
                res.render('/academyProduct/academyproducterror');
            }

            /*dw.system.HookMgr.callHook( "app.hookacademy", "productData", product );
            res.render('/academyProduct/academyproduct')*/
        }else{
            res.render('/academyProduct/academyproducterror');
        }
        next();
    }
);

module.exports = server.exports();