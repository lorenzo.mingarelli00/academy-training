'use strict';

//Use the following for CSRF protection: add middleware in routes and hidden field on form
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');
var server = require('server');
var URLUtils = require('dw/web/URLUtils');

server.get(
    'Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req, res, next) {
        var x = server.forms;
        var newsletterForm = server.forms.getForm('newsletter');
        newsletterForm.clear();

        res.render('/newsletter/newslettersignup', {
            actionUrl: dw.web.URLUtils.url('Newsletter-Create'),
            newsletterForm: newsletterForm
        });

        next();
    }
);

server.post('Create', server.middleware.https, function (req, res, next) {
        var newsletterHelpers = require('*/cartridge/scripts/helpers/newsletterHelpers');
        var newsletterForm = server.forms.getForm('newsletter');
        var service = newsletterHelpers.getService('crm.newsletter.subscribe');

        var email = newsletterForm.email.htmlValue;
        var name = newsletterForm.fname.htmlValue;
        var surname = newsletterForm.lname.htmlValue;
        
        var reqObject = {
            name: name, 
            surname: surname,
            email: email
        };
        var response = service.call(reqObject);

        res.json({  
            name: name,
            surname: surname, 
            email: email,
            response: response.ok ? response.msg : 'there was an error: '+ response.msg,
            success: response.ok,
            redirectUrl: dw.web.URLUtils.url('Newsletter-Success').toString()
        });
        next();
});

server.get(
    'Success',
    server.middleware.https,
    function (req, res, next) {
        res.render('/newsletter/newslettersuccess', {
            continueUrl: URLUtils.url('Newsletter-Show'),
            newsletterForm: server.forms.getForm('newsletter')
        });

        next();
    }
);

server.post(
    'Handler',
    server.middleware.https,
    csrfProtection.validateAjaxRequest,
    function (req, res, next) {
        var newsletterForm = server.forms.getForm('newsletter');
        var continueUrl = dw.web.URLUtils.url('Newsletter-Show');

        // Perform any server-side validation before this point, and invalidate form accordingly
        if (newsletterForm.valid) {
            // Send back a success status, and a redirect to another route
            res.json({
                success: true,
                redirectUrl: URLUtils.url('Newsletter-Success').toString()
            });
        } else {
            // Handle server-side validation errors here: this is just an example
            res.setStatusCode(500);
            res.json({
                error: true,
                redirectUrl: URLUtils.url('Error-Start').toString()
            });
        }
        next();
    }
);

module.exports = server.exports();