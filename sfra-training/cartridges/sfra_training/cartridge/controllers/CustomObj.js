'use strict';

var server = require('server');
var URLUtils = require('dw/web/URLUtils');
var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');
var iterator = require('dw/util/SeekableIterator');

server.get(
    'Create',
    server.middleware.https,
    function (req, res, next) {
        var currentTimeStamp = stringUtils.formatCalendar(calendar(new Date()), "yyyy-MM-dd-HH:mm:ss:SS");
        var jsonResponse = {};

        if (dw.system.Site.getCurrent().getCustomPreferenceValue("isCreateObject")) {
            
            try {
                transaction.wrap(function () {
                    if (dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectType") == null) {
                        jsonResponse.success = false;
                        jsonResponse.error = "newsletterObjectType is null";
                        res.json(jsonResponse);
                        next();
                        return;
                    }

                    var customObject = customObjectMgr.createCustomObject(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectType"), currentTimeStamp);

                    if (req.querystring.Nome != null &&
                        req.querystring.Cognome != null &&
                        req.querystring.Email != null) {
                        customObject.custom.Nome = req.querystring.nome;
                        customObject.custom.Cognome = req.querystring.cognome;
                        customObject.custom.Email = req.querystring.email;

                        jsonResponse.succes = true;
                        jsonResponse.idcust = customObject.custom.timestamp;
                    }else{
                        jsonResponse.success = false;
                        jsonResponse.error = "I valori passati tramite URL devono essere tutti presenti e diversi da null";
                    }
                    
                });
            } catch (error) {
                jsonResponse.success = false;
                jsonResponse.error = "Exception triggered";
            }
        }else{
            jsonResponse.success = false;
            jsonResponse.error = "isCreateObject is disabled";
        }
        
        res.json(jsonResponse);
        next();
    }
);

server.get(
    'List',
    server.middleware.https,
    function (req, res, next){
        var list = customObjectMgr.getAllCustomObjects(dw.system.Site.getCurrent().getCustomPreferenceValue("newsletterObjectType"));
        

        var array = [];
        var jsonResponse = {};
        while(list.hasNext()){
            var item = list.next();
            array.push({
                "ID" : item.custom.timestamp,
                "Nome" : item.custom.Nome,
                "Cognome" : item.custom.Cognome,
                "Email" : item.custom.Email
            });
        }
        list.close();

        jsonResponse.items = array;
        res.json(jsonResponse);
        next();
    }
);

server.get(
    'Delete',
    server.middleware.https,
    function (req, res, next){
        var id = req.querystring.id;
        var itemToDelete = customObjectMgr.getCustomObject('newsletter1', id);
        var jsonResponse = {};

        try {
            transaction.wrap(function () {
                customObjectMgr.remove(itemToDelete);
                jsonResponse.success = true;
            });
        } catch (error) {
            jsonResponse.success = false;
            jsonResponse.error = "Exception triggered";
        }

        res.json(jsonResponse);
        next();
    }
);

server.get(
    'PayMet',
    server.middleware.https,
    function (req, res, next){
        PaymentMgr.getActivePaymentMethods();
        











    }
)

module.exports = server.exports();
