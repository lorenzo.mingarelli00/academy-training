'use strict';

var stringUtils = require('dw/util/StringUtils');
var calendar = require('dw/util/Calendar');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');

function productData(product){
    var currentTimeStamp = stringUtils.formatCalendar(calendar(new Date()), "yyyy-MM-dd-HH:mm:ss:SS");

    var id = product.ID;
    var name = product.name;
    var shortDes = product.shortDescription;

    try {
        transaction.wrap(function () {
            var customObject = customObjectMgr.createCustomObject('academyProduct', currentTimeStamp);

            customObject.custom.nome = name;
            customObject.custom.id = id;
            customObject.custom.shortDescription = shortDes;
        });
    } catch (error) {}

    return{
        ID : id,
        Name : name,
        ShortDescription : shortDes
    };
}

exports.productData = productData;
