'use strict';

var status = require('dw/system/Status');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');

var jobTest = function (){
    var args = arguments [0];
    /*var objectData = {};
    objectData.key = args.TestParam;*/
    var id = args.TestParam;

    try {
        transaction.wrap(function () {
            var itemToDelete = customObjectMgr.getCustomObject('academyProduct', id);

            if(itemToDelete != null){
                customObjectMgr.remove(itemToDelete);
                return new status(status.OK);
            }else{
                return new status(status.ERROR);
            }
        });
    } catch (error) {}
}

exports.jobTest = jobTest;
