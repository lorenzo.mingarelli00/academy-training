'use strict';

var status = require('dw/system/Status');
var customObjectMgr = require('dw/object/CustomObjectMgr');
var transaction = require('dw/system/Transaction');
var iterator = require('dw/util/SeekableIterator');
var logger = require('dw/system/Logger');

var printCustomObjects = function (){
    var customLogger = logger.getLogger('printCO','error');
    var args = arguments[0];
    var type = args.ObjectsType;

    var list = customObjectMgr.getAllCustomObjects(type);
        
    var array = [];

    while(list.hasNext()){
        var item = list.next();
        var id = item.custom.id;
        var nome =  item.custom.nome;
        var shortDes = item.custom.shortDescription;

        var toStamp = id+ ', ' +nome+ ', ' +shortDes;
        customLogger.error(toStamp);

        /*array.push(item.custom.id);
        array.push({
            "ID" : item.custom.id,
            "Nome" : item.custom.nome,
            "BreveDescrizione" : item.custom.shortDescription
        });*/
    }
    list.close();

    //customLogger.error('Lista:',array);
    return new status(status.OK);
}

exports.printCustomObjects = printCustomObjects;
