'use strict';

var localServiceRegistry = require('dw/svc/LocalServiceRegistry');

/**
 * Fetches the locals service registry assigned to a service id
 * @param {String} serviceId - service id
 * @returns {Object} a local service registry
 */
function getService(serviceId) {
    var service = LocalServiceRegistry.createService("reCaptchav3",{
        createRequest: function (service) {
            service.URL = service.URL + '?secret=' + svcparams.secret + '&response=' + svcparams.response;
            service.requestMethod = "GET";
            service.addHeader("Content-Type", "application/json");
            service.addHeader("Accept","application/json");
        },
        parseResponse: function (service) {
            return service.getClient().text;
        }
    });
}

/*
    return localServiceRegistry.createService(serviceId, {
        createRequest: function(svc, args) {
            // Default request method is post
            // No need to setRequestMethod
            if (args) {
                svc.addHeader("Content-Type", "text/json");
                return JSON.stringify(args);
            } else {
                return null;
            }
        },
        parseResponse: function(svc, client) {
            return client.text;
        },
        mockCall: function(svc, client) {
            return {
                statusCode: 200,
                statusMessage: "Success",
                text: "MOCK RESPONSE (" + svc.URL + ")"
            };
        }
    });
 */

module.exports = {
    getService: getService
};

